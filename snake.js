var game_canvas = document.getElementById("game")
var ctx = game_canvas.getContext("2d")

const WORLD_ELEMENTS = {
    EMPTY: 0,
    SNAKE: 1,
    FOOD: 2,
    WALL: 3
}

const CHECK_RESULTS = {
    OUT_OF_BOUNDS: 0,
    INVALID_DIRECTION: 1,
    ON_SNAKE_BODY: 2,
    ON_FOOD: 3,
    OK: 4
}

var SNAKE = [
    [4, 2],
    [4, 1],
    [3, 1],
    [2, 1]
]

var FOOD = [0, 0]

var canvas_size = [300, 300]
var grid_size = [30, 30]

var next_direction = "down"

var game_interval = null

document.addEventListener("keydown", function(e) {
    switch (e.code) {
        case "ArrowDown":
            if (next_direction != "up") next_direction = "down"
            break
        case "ArrowUp":
            if (next_direction != "down") next_direction = "up"
            break;
        case "ArrowLeft":
            if (next_direction != "right") next_direction = "left"
            break
        case "ArrowRight":
            if (next_direction != "left") next_direction = "right"
            break
    }
})

function game_init() {
    draw_snake()
    choose_food_randomly()
    draw_food()
    game_interval = setInterval(game_step, 200)
}

function draw_snake() {
    ctx.fillStyle = "black"
    for (let i = 0; i < SNAKE.length; i++) {
        ctx.fillRect(SNAKE[i][0] * (canvas_size[0] / grid_size[0]), SNAKE[i][1] * (canvas_size[1] / grid_size[1]), (canvas_size[0] / grid_size[0]), (canvas_size[1] / grid_size[1]))
    }
}

function draw_food() {
    ctx.fillStyle = "green"
    ctx.fillRect(FOOD[0] * (canvas_size[0] / grid_size[0]), FOOD[1] * (canvas_size[1] / grid_size[1]), (canvas_size[0] / grid_size[0]), (canvas_size[1] / grid_size[1]))
}

function check_next_head(new_head) {
    if (new_head[0] < 0 || new_head[1] < 0 || new_head[0] >= grid_size[0] || new_head[1] >= grid_size[1]) {
        return CHECK_RESULTS.OUT_OF_BOUNDS // out of bounds
    }

    if ((does_snake_include(new_head[0], new_head[1]))) {
        return CHECK_RESULTS.ON_SNAKE_BODY
    }

    if (new_head[0] == FOOD[0] && new_head[1] == FOOD[1]) {
        return CHECK_RESULTS.ON_FOOD
    }

    /* check if not opposite direction
	if (new_head[0] == SNAKE[SNAKE.length - 2][0] && new_head[1] == SNAKE[SNAKE.length - 2][1]) {
        return CHECK_RESULTS.INVALID_DIRECTION
	}
	*/
    return
}

function game_step() {
    let head = SNAKE[SNAKE.length - 1]
    let new_head = [head[0], head[1]] // copy of head

    switch (next_direction) {
        case "up":
            new_head[1]--
                break
        case "down":
            new_head[1]++
                break
        case "left":
            new_head[0]--
                break
        case "right":
            new_head[0]++
                break
    }

    let is_food = false

    switch (check_next_head(new_head)) {
        case CHECK_RESULTS.OUT_OF_BOUNDS:
            game_over()
            return
        case CHECK_RESULTS.ON_SNAKE_BODY:
            game_over()
            return
        case CHECK_RESULTS.ON_FOOD:
            is_food = true
            break
    }

    if (!is_food) { // IF NEXT CASE IS NOT FOOD THEN SHIFT THE SNAKE
        SNAKE.shift()
    }

    SNAKE.push(new_head)

    if (is_food) { // IF THE NEXT CASE IS FOOD, THEN CHOOSE NEX VALUE FOR FOOD CASE
        choose_food_randomly()
    }

    is_food = false

    ctx.clearRect(0, 0, game_canvas.width, game_canvas.height)
    ctx.fillStyle = "red"
    ctx.fillRect(0, 0, game_canvas.width, game_canvas.height)
    draw_snake();
    draw_food();
}

function game_over() { // END THE GAME AND DISPLAY MESSAGE
    document.getElementById("status").innerText = "Game Over"
    clearInterval(game_interval)
}

function does_snake_include(i, j) { // CHECK IF SNAKE IS ONT THE CASE
    for (let k = 0; k < SNAKE.length; k++) {
        if (SNAKE[k][0] == i && SNAKE[k][1] == j) return true
    }
    return false
}

function choose_food_randomly() { // SET FOOD VAR WITH A RANDOMLY CHOSEN VALUE THAT IS AVAILABLE
    let empty_space_array = []
    for (let i = 0; i < grid_size[0]; i++) {
        for (let j = 0; j < grid_size[1]; j++) {
            if (!does_snake_include(i, j)) {
                empty_space_array.push([i, j])
            }
        }
    }

    FOOD = empty_space_array[Math.floor(Math.random() * empty_space_array.length)]
}

game_init()